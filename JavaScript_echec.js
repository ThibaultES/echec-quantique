
// ----------------------------- Variables globales ------------------------------------

let RoiIntrique=true; // option de fin de partie  -true : roi peut etre intrique, Matt =roi mangé / -false : roi peut pas etre intrique, Matt =meme que echec normal

let DepP=       {}; // indique si une piece a deja ete deplacee
let DernierCoup={'piece':"",'proba':"",'depart':"",'arrivee':"",'arrivee2':null,'pionDouble':false,'pionDouble2':false};
let Echec=      {}; //Le roi est en échec : Echec[('roi','<couleur>')]=True

let dico_src_N = {tour : "image/pieces/Tour-N.png",cavalier : "image/pieces/Cavalier-N.png" , fou : "image/pieces/Fou-N.png", roi : "image/pieces/Roi-N.png" , dame : "image/pieces/Dame-N.png" , pion : "image/pieces/Pion-N.png" };
let dico_src_B = {tour : "image/pieces/Tour-B.png",cavalier : "image/pieces/Cavalier-B.png" , fou : "image/pieces/Fou-B.png", roi : "image/pieces/Roi-B.png" , dame : "image/pieces/Dame-B.png" , pion : "image/pieces/Pion-B.png" };
let dico_id_N = {tour : "TN" ,cavalier : "CN" , fou : "FN", roi : "RN" , dame : "DN" , pion : "PN"  };
let dico_id_B = {tour : "TB" ,cavalier : "CB" , fou : "FB", roi : "RB" , dame : "DB" , pion : "PB"  };



var selections = [];   
let tour = 0;   // Juste le nombre de tours de jeu 
let nb_max_intrication = 100 ;
let nb_intrications = 0;
let division = 0.5;
let choisir_probabilités = false;
let chronos_active = true;


// HTML Elements 
let c0  = document.getElementById('0') ;let c1  = document.getElementById('1') ;let c2  = document.getElementById('2') ;let c3  = document.getElementById('3') ;let c4  = document.getElementById('4') ;let c5  = document.getElementById('5') ;let c6  = document.getElementById('6') ;let c7  = document.getElementById('7') ;
let c8  = document.getElementById('8') ;let c9  = document.getElementById('9') ;let c10 = document.getElementById('10');let c11 = document.getElementById('11');let c12 = document.getElementById('12');let c13 = document.getElementById('13');let c14 = document.getElementById('14');let c15 = document.getElementById('15');
let c16 = document.getElementById('16');let c17 = document.getElementById('17');let c18 = document.getElementById('18');let c19 = document.getElementById('19');let c20 = document.getElementById('20');let c21 = document.getElementById('21');let c22 = document.getElementById('22');let c23 = document.getElementById('23');
let c24 = document.getElementById('24');let c25 = document.getElementById('25');let c26 = document.getElementById('26');let c27 = document.getElementById('27');let c28 = document.getElementById('28');let c29 = document.getElementById('29');let c30 = document.getElementById('30');let c31 = document.getElementById('31');
let c32 = document.getElementById('32');let c33 = document.getElementById('33');let c34 = document.getElementById('34');let c35 = document.getElementById('35');let c36 = document.getElementById('36');let c37 = document.getElementById('37');let c38 = document.getElementById('38');let c39 = document.getElementById('39');
let c40 = document.getElementById('40');let c41 = document.getElementById('41');let c42 = document.getElementById('42');let c43 = document.getElementById('43');let c44 = document.getElementById('44');let c45 = document.getElementById('45');let c46 = document.getElementById('46');let c47 = document.getElementById('47');
let c48 = document.getElementById('48');let c49 = document.getElementById('49');let c50 = document.getElementById('50');let c51 = document.getElementById('51');let c52 = document.getElementById('52');let c53 = document.getElementById('53');let c54 = document.getElementById('54');let c55 = document.getElementById('55');
let c56 = document.getElementById('56');let c57 = document.getElementById('57');let c58 = document.getElementById('58');let c59 = document.getElementById('59');let c60 = document.getElementById('60');let c61 = document.getElementById('61');let c62 = document.getElementById('62');let c63 = document.getElementById('63');

let case_annexe = 		document.getElementById('info_case');
let image_contenu_case =document.createElement('img');
let boutton_menu = 		document.getElementById('bouton_menu');
let bouton_opts_avance =document.getElementById('bouton_opts_avance');
let menu_opts = 		document.getElementById('menu_options');
let menu_opts_avance = 	document.getElementById('opts_avance');
let nb_piece = 			document.getElementById('nb_piece');
let nb_intric = 		document.getElementById('nb_intric');
let max_intric = 		document.getElementById('max_intric');
let nb_tour = 			document.getElementById('nb_tour');
let choix_proba = 		document.getElementById('choix_proba');
let activ_chrono =		document.getElementById('activ_chrono');
let setChr =			document.getElementById('setChr');
let boutton_jouer = 	document.getElementById('Jouer');
let recommencer = 		document.getElementById('newGame');
let zone_morts_B = 		document.getElementById('P_mortes_B'); 
let zone_morts_N = 		document.getElementById('P_mortes_N')  ;






// ----------------------------- Classes Piece/Case/Plateau -------------------------------


class Piece {
	constructor(nature,couleur,num) {  
		this.type = nature;  
		this.couleur=couleur;
		this.id=num;
	}
	
	repr(){
	//let s= String(this.couleur);
	return this.type + ";" + this.couleur + ";" + this.id;
	}

	deplacementsPos(){
	if (this.type == 'pion' && this.couleur=='blanc'){return [0,7,1];
	} else if(this.type == 'pion' && this.couleur=='noir'){return [4,3,5];
	} else if(this.type == 'fou'){return [7,1,3,5];
	} else if(this.type == 'cavalier'){return [[0,7],[0,1],[2,1],[2,3],[4,3],[4,5],[6,5],[6,7]];
	} else if(this.type == 'tour'){return [0,2,4,6];
	} else if(this.type == 'dame'){return [7,0,1,2,3,4,5,6];
	} else if(this.type == 'roi'){return [7,0,1,2,3,4,5,6];}
	}

	egal(other){
	if (this.type==other.type && this.couleur==other.couleur && this.id==other.id){return true;
	} else {return false;};
	}
}


let roi_N= new Piece('roi','noir',15);
let roi_B= new Piece('roi','blanc',16);



class Case{
	constructor(pieces,num){
		this.P=pieces;
		this.num=num;
	}

	repr(){
	return this.num + " ; " + this.P ;
	}

	ajoutePiece(piece,proba){
	this.P.push([proba,piece]);
	DepP[piece.id] = true;
	}

	enlevePiece(piece){
	for (var i=0; i<this.P.length; i++){
		if (this.P[i][1].egal(piece)){
			this.P.splice(i,1);
			
			return;
			}
		}
	}

}


class Plateau{
	constructor(cases){
	this.cases=cases
	}

	repr(){
	let l=[];
	for (var i=0; i<64; i++){
		l.push(this.cases[i].repr());};
	return l;
	}

	voisines(c){
	if (c==0){
		return [null,null,1,9,8,null,null,null]
	} else if(c==7){
		return [null,null,null,null,15,14,6,null]
	} else if(c==56){
		return [48,49,57,null,null,null,null,null]
	} else if(c==63){
		return [55,null,null,null,null,null,62,54]
	} else if(c>0 && c<8){
		return [null,null,c+1,c+9,c+8,c+7,c-1,null]
	} else if(c>57 && c<63){
		return [c-8,c-7,c+1,null,null,null,c-1,c-9]
	} else if(c==8 || c==16 || c==24 || c==32 || c==40 || c==48){
		return [c-8,c-7,c+1,c+9,c+8,null,null,null]
	} else if(c==15 || c==23 || c==31 || c==39 || c==47 || c==55){
		return [c-8,null,null,null,c+8,c+7,c-1,c-9]
	} else {
		return [c-8,c-7,c+1,c+9,c+8,c+7,c-1,c-9];
	}
	}
	

	voisineD(c,d){
	return this.voisines(c)[d]
	}

	desintrique(piece){
	let n=[];
	for (var i=0; i<64; i++){
		for (var j=0; j<this.cases[i].P.length ; j++){
			if (this.cases[i].P[j][1].egal(piece)){
				n.push([i,this.cases[i].P[j][0]]);
			}
		}
	};
	let s=0;
	for (let k=0; k<n.length;k++){
		s=s+n[k][1];
	};
	

	let r= Math.random();

	if (n.length == 0){
		return;
	} else if (n.length>0){
		let g=false;
		let h=0;
		let a=0;
		let b=0;
		while ( g==false && h<n.length){
			
			a=b;
			
			b= b+Number(n[h][1]);
			
			if (r>= a && r<b){ g=true};
			h=h+1;
		}
		
		h= h-1;
		
		if (g==true){
			
			this.cases[n[h][0]].P=[[1,new Piece(piece.type,piece.couleur,piece.id)]];
			for (let t=0;t<n.length;t++){
				if (t!=h){this.cases[n[t][0]].enlevePiece(piece)}
			}
		} else {
			
			for (let t=0;t<n.length;t++){
				this.cases[n[t][0]].enlevePiece(piece);
			}
		}
		
		
	}
	
	}
	
	
	ajouterPiece(c,piece,proba){
		
	if (this.cases[c].P.length==0){
		this.cases[c].ajoutePiece(piece,proba);
		return;
	}
	
	for (let i=0;i<this.cases[c].P.length;i++){
		if (this.cases[c].P[i][1].egal(piece)){
			this.cases[c].P[i]=[this.cases[c].P[i][0]+proba, this.cases[c].P[i][1]];
			if (this.cases[c].P.length==2){
				let j=this.cases[c].P[i][0]+this.cases[c].P[1-i][0];
				if (j>1){
					this.cases[c].P[1-i]=[this.cases[c].P[1-i][0]-(j-1),this.cases[c].P[1-i][1]];
				};
				if (this.cases[c].P[1-i][0]<=0){
					this.cases[c].enlevePiece(this.cases[c].P[1-i][1]);
				};
			};
			return;
		}
	}
	
	if (this.cases[c].P.length==1 && this.cases[c].P[0][0]==1){
		
		let Piece2=this.cases[c].P[0][1];
		if (Piece2.couleur==piece.couleur){
			let texte="Can't eat";
			return texte;
		};
		this.cases[c].enlevePiece(Piece2);
		this.cases[c].ajoutePiece(piece,proba);
		if (proba<1){
			this.cases[c].ajoutePiece(Piece2,1-proba);
		} else {repas(piece,c);}
		return;
	} else {
		
		this.desintrique(this.cases[c].P[0][1]);
        this.ajouterPiece(c,piece,proba);
        return;
	}
	
	}
	
	
	deplacerPiece(piece,proba,depart,arrivee){
		
		this.cases[depart].enlevePiece(piece);
		let a=this.ajouterPiece(arrivee,piece,proba);
		if (a=="Can't eat"){
			this.ajouterPiece(depart,piece,proba);
		}
		return a;
	}
	


}










//------------------------------------ fonctions ----------------------------------


// ---------------------- Fonctions creation Pieces/plateau -------------------------

function plateauVide(){
    let C=[];
    for (let i=0;i<64;i++){
        C.push(new Case([],i));
	};
    return new Plateau(C);
}


let plateau=plateauVide();
plateauDepart();

function CreePiece(c,piece,proba){
	plateau.ajouterPiece(c,piece,proba);
	DepP[piece.id]=false;
	if (piece.type=='roi'){
		if (piece.couleur=='blanc'){
			Echec[roi_B]=false;
		} else {
			Echec[roi_N]=false;
		};
	}
}

function plateauDepart(){
    for (let i=0;i<8;i++){CreePiece(i+8,new Piece('pion','noir',i),1)};
    for (let i=0;i<2;i++){CreePiece([0,7][i],new Piece('tour','noir',8+i),1)};
    for (let i=0;i<2;i++){CreePiece([1,6][i],new Piece('cavalier','noir',10+i),1)};
    for (let i=0;i<2;i++){CreePiece([2,5][i],new Piece('fou','noir',12+i),1)};
    CreePiece(3,new Piece('dame','noir',14),1);
    CreePiece(4,roi_N,1);
    
    for (let i=0;i<8;i++){CreePiece(i+48,new Piece('pion','blanc',17+i),1)};
    for (let i=0;i<2;i++){CreePiece([56,63][i],new Piece('tour','blanc',25+i),1)};
    for (let i=0;i<2;i++){CreePiece([57,62][i],new Piece('cavalier','blanc',27+i),1)};
    for (let i=0;i<2;i++){CreePiece([58,61][i],new Piece('fou','blanc',29+i),1)};
    CreePiece(59,new Piece('dame','blanc',31),1);
    CreePiece(60,roi_B,1);
    
    return plateau;
}


// -------------------------- Fonctions Deplacements Pieces --------------------------


function intrication(piece1,piece2,proba,depart,arrivee1,arrivee2){
	if (choisir_probabilités==false) {division=proba/2}
	if (RoiIntrique==false && (piece1.type=='roi' || piece2.type=='roi')){return;};
	let a=plateau.deplacerPiece(piece1,division,depart,arrivee1);
	if (a=="Can't eat"){return a;};
    return plateau.deplacerPiece(piece2,1-division,depart,arrivee2);
}


function deplace(piece,depart,arrivee,arrivee2=null,essai=false,proba=null){
	
	if (depart==arrivee || depart==arrivee2){
		return false;
	}
	
	let a='';
	let Possible=[];
	//let proba=0;
	if (proba==null){
		for (let i=0;i<plateau.cases[depart].P.length;i++){
			if (plateau.cases[depart].P[i][1].egal(piece)==true){
				proba=plateau.cases[depart].P[i][0];
			}
		}
	}
	
	
	if (piece.type=='pion'){ // --- PION ---
		
		for (let j=0;j<piece.deplacementsPos().length;j++){
			Possible.push(plateau.voisineD(depart,piece.deplacementsPos()[j]));
		}
		
		if (piece.couleur=='blanc'){
			Possible.push(plateau.voisineD(plateau.voisineD(depart,0),0));
		} else {
			Possible.push(plateau.voisineD(plateau.voisineD(depart,4),4));
		}
		
		if (fonctionPionPeut(piece,Possible,depart,arrivee)[0]==false){return false;}
		if (fonctionPionPeut(piece,Possible,depart,arrivee)[1]=='double'){
			if (essai==false){DernierCoup['pionDouble']=true;}
		}
		
		if (fonctionPionPeut(piece,Possible,depart,arrivee)[1]=='PEP'){
			if (piece.couleur=='blanc'){
				if (essai==false){plateau.cases[plateau.voisineD(arrivee,4)].enlevePiece(DernierCoup['piece']);}
			} else if (piece.couleur=='noir'){
				if (essai==false){plateau.cases[plateau.voisineD(arrivee,0)].enlevePiece(DernierCoup['piece']);}
			}
		}
		
		if ((piece.couleur=='blanc' && [0,1,2,3,4,5,6,7].includes(arrivee)) || (piece.couleur=='noir' && [56,57,58,59,60,61,62,63].includes(arrivee))){
			nvPiece1=pionDame(piece);
		} else {
			nvPiece1=piece;
			
		}
		
		if (arrivee2!=null){
			if (fonctionPionPeut(piece,Possible,depart,arrivee2)[0]==false){
				return false;
			} else {
				if (fonctionPionPeut(piece,Possible,depart,arrivee2)[1]=='double'){
					if (essai==false){DernierCoup['pionDouble2']=true}
				}
				if (fonctionPionPeut(piece,Possible,depart,arrivee2)[1]=='PEP'){
					if (piece.couleur=='blanc'){
						if (essai==false){plateau.cases[plateau.voisineD(arrivee,4)].enlevePiece(DernierCoup['piece']);}
					} else if (piece.couleur=='noir'){
						if (essai==false){plateau.cases[plateau.voisineD(arrivee,0)].enlevePiece(DernierCoup['piece']);}
					}
				}
				
				if ((piece.couleur=='blanc' && [0,1,2,3,4,5,6,7].includes(arrivee2)) || (piece.couleur=='noir' && [56,57,58,59,60,61,62,63].includes(arrivee2))){
					nvPiece2=pionDame(piece);
				} else {nvPiece2=piece;}
				
				if (essai==false){a=intrication(nvPiece1,nvPiece2,proba,depart,arrivee,arrivee2);}
				if (nvPiece1!=piece || nvPiece2!=piece){
					plateau.cases[depart].enlevePiece(piece);
				}
			}
		} else {
			
			if (essai==false){a = plateau.deplacerPiece(nvPiece1,proba,depart,arrivee);}
			if (nvPiece1!=piece){
				plateau.cases[depart].enlevePiece(piece);
			}
		}
		
		if (a=="Can't eat"){
			return false;
		} else {
			DernierCoup['piece']=piece;
			DernierCoup['proba']=proba;
			DernierCoup['depart']=depart;
			DernierCoup['arrivee']=arrivee;
			if (arrivee2!=null){
				DernierCoup['arrivee2']=arrivee2;
			}
			return true;
		}
		
	} else if (piece.type=='roi'){ // --- ROI ---
		
		for (let j=0;j<piece.deplacementsPos().length;j++){
			let i=piece.deplacementsPos()[j];
			let Case=plateau.voisineD(depart,i);
			if (Case!=null && Case>=0 && Case<64){
				if (contient(Case,null,piece.couleur,null)[0]==false){
					Possible.push(Case);
				}
			}
		}
		
				
	
			
	
		
		
		if (Possible.includes(arrivee)){
			
			if (arrivee2==null || RoiIntrique==false){
				if (essai==false){let a=plateau.deplacerPiece(piece,proba,depart,arrivee);}
			}else if (Possible.includes(arrivee2)){
				if (essai==false){let a=intrication(piece,piece,proba,depart,arrivee,arrivee2);}
			}else{
				return false;
			}
			if (a=="Can't eat"){
				return false;
			
			} else {
				DernierCoup['piece']=piece;
				DernierCoup['proba']=proba;
                DernierCoup['depart']=depart;
                DernierCoup['arrivee']=arrivee;
				if (arrivee2!=null){DernierCoup['arrivee2']=arrivee2;}
                return true;
			}
			
		} else {
			if (rock(piece,depart,arrivee)==true){
				
				DernierCoup['piece']=piece;
                DernierCoup['proba']=proba;
                DernierCoup['depart']=depart;
                DernierCoup['arrivee']=arrivee;
                if (arrivee2!=null){DernierCoup['arrivee2']=arrivee2}
				return true;
			} else {
				return false;
			}
		}
		
		
		
	} else if (piece.type=='cavalier'){ // --- Cavalier ---
		
		for (let i=0;i<(piece.deplacementsPos()).length;i++){
			j=piece.deplacementsPos()[i];
			if (plateau.voisineD(depart,j[0])!=null){
				let Case=plateau.voisineD(plateau.voisineD(depart,j[0]),j[1]);
				if (Case!=null && Case>=0 && Case<64){
					if (contient(Case,null,piece.couleur,null)[0]==false){
						Possible.push(Case);
					}
				}
			}
		}
		
		if (Possible.includes(arrivee)){
			
			if (arrivee2==null){
				
				if (essai==false){let a=plateau.deplacerPiece(piece,proba,depart,arrivee);}
			}else if (Possible.includes(arrivee2)){
				if (essai==false){let a=intrication(piece,piece,proba,depart,arrivee,arrivee2);}
			}else{
			
				return false;
			}
			if (a=="Can't eat"){
				return false;
			} else {
				DernierCoup['piece']=piece;
                DernierCoup['proba']=proba;
                DernierCoup['depart']=depart;
                DernierCoup['arrivee']=arrivee;
                if (arrivee2!=null){DernierCoup['arrivee2']=arrivee2;};
                return true;
			}
		} else {
			
			return false;
		}
			
	} else { // --- FOU / TOUR / DAME ---
		
		let autreCouleur='';
		if (piece.couleur=='blanc'){
			autreCouleur='noir';
		} else {
			autreCouleur='banc';
		}
		
		for (let j=0;j<piece.deplacementsPos().length;j++){
			let i=piece.deplacementsPos()[j];
			
			let Case=plateau.voisineD(depart,i);
			
			while (Case != null && Case<64 && contient(Case,null,piece.couleur,null)[0]==false && contient(Case,null,autreCouleur,null)[0]==false){
				//console.log('-');
				//console.log(Case);
				//console.log(contient(Case,null,piece.couleur,null));
				Possible.push(Case);
				Case=plateau.voisineD(Case,i);
			}
			if (Case !=null){
				if (contient(Case,null,autreCouleur,null)[0]==true){
					Possible.push(Case);
				} else if(contient(Case,piece.type,piece.couleur,piece.id)[0]==true){
					Possible.push(Case);
				}
			}
		}
	
		
		
		if (Possible.includes(arrivee)){
			
			if (arrivee2==null){
				if (essai==false){let a=plateau.deplacerPiece(piece,proba,depart,arrivee);}
			} else if (Possible.includes(arrivee2)) {
				if (essai==false){let a=intrication(piece,piece,proba,depart,arrivee,arrivee2);}
			} else {
				return false;
			}
			
			if (a=="Can't eat"){
				return false;
			} else {
				DernierCoup['piece']=piece;
                DernierCoup['proba']=proba;
                DernierCoup['depart']=depart;
                DernierCoup['arrivee']=arrivee;
                if (arrivee2!=null){DernierCoup['arrivee2']=arrivee2;};
                return true;
			}
		} else {
			return false;
		}
            
	}

}


function rock(piece,depart,arrivee){
	
	if (DepP[piece.id]==false){
		if ([depart-4,depart+3].includes(arrivee)==false) {return false;}
		
	
	
		let T=recherche_p(arrivee,'tour',piece.couleur)
	
		if (T==false){return false;}
		if (T[0]!=1){
			return false;
		} else if (plateau.cases[arrivee].P.length>1){
			return false;
		}
	
		if (echec(depart,piece.couleur)==true){return false;}
	
		if (arrivee==depart-4){
			if (echec(depart-1,piece.couleur)==true || echec(depart-2,piece.couleur)==true){return false;};
		} else if (arrivee==depart+3){
			if (echec(depart+1,piece.couleur)==true || echec(depart+2,piece.couleur)==true){return false;};
		}
		
	
		if (arrivee==depart-4){
			for (let i=0;i<3;i++){
			let j=depart-(3-i);
			if (plateau.cases[j].P.length != 0){
					return false;
				}
			
			}
		} else if (arrivee==depart+3){
			
			for (let i=1;i<3;i++){
				let j=depart+i;
				if (plateau.cases[j].P.length != 0){
					return false;
				}
			}
		}
		
		if (arrivee==depart+3){
			
			plateau.deplacerPiece(piece,1,depart,arrivee-1);
			plateau.deplacerPiece(T[1],1,arrivee,depart+1);
			return true;
		}
		if (arrivee==depart-4){
        	plateau.deplacerPiece(piece,1,depart,arrivee+2);
        	plateau.deplacerPiece(T[1],1,arrivee,depart-1);
			return true;
		}
	
	} else {return false;}
}


function echec(ca,couleur){
	for (let c=0;c<plateau.cases.length;c++){
		for (let p=0;p<plateau.cases[c].P.length;p++){
			if (plateau.cases[c].P[p][1].couleur!=couleur){
				if (deplace(plateau.cases[c].P[p][1],c,ca,essai=true)==true){
					return true;
				}
			}
		}
	}
	return false;
}


function fonctionPionPeut(piece,possible,d,a){
	let g=false;
	let A=0;
	
	if (a==possible[0]){
		if (plateau.cases[a].P.length==0){
			return [true,null];
		}
		for (let i=0; i<plateau.cases[a].P.length;i++){
			if (contient(a,piece.type,piece.couleur,piece.id)[0]==true){
				return [true,null];
			}
		}
		return [false,null];
		
	}
	
	if (a==possible[3] && ((piece.couleur=='blanc' && [48,49,50,51,52,53,54,55].includes(d)) || (piece.couleur=='noir' && [8,9,10,11,12,13,14,15].includes(d)))){
		return [true,'double'];
	}
	
	if (DernierCoup['pionDouble']==true){
		A=DernierCoup['arrivee'];
		g=true;
	} else if (DernierCoup['pionDouble2']==true){
		A=DernierCoup['arrivee2'];
		g=true;
	}
	
	if (g==true){
		if ((d==plateau.voisineD(A,2) || d==plateau.voisineD(A,6)) && piece.couleur!=DernierCoup['piece'].couleur){
			if ((piece.couleur=='blanc' && a==plateau.voisineD(A,0)) || (piece.couleur=='noir' && a==plateau.voisineD(A,4))){
				return [true,'PEP'];
			}
		}
	}
	
	if (a==possible[1]){
		for (let i=0;i<plateau.cases[possible[1]].P.length;i++){
			if (plateau.cases[possible[1]].P[i][1].couleur != piece.couleur){
				return [true,null];
			}
		}
	}

	if (a==possible[2]){
		for (let i=0;i<plateau.cases[possible[2]].P.length;i++){
			if (plateau.cases[possible[2]].P[i][1].couleur != piece.couleur){
				return [true,null];
			}
		}
	}
	
	
	return [false,null];
}



function pionDame(piece){
	let n=prompt('Le pion devient : \n- une dame (0)\n- un fou (1)\n- un cavalier (2)\n- une tour (3)');
	let np='';
	if (n=='0'){
		np='dame';
	} else if (n=='1'){
		np='fou';
	} else if (n=='2'){
		np='cavalier';
	} else if (n=='3'){
		np='tour';
	} else {
		alert("Vous devez donner un nombre valide !");
		return pionDame(piece);
	}
	
	let NP=new Piece(np,piece.couleur,0)
	let g=[];
	for (let c=0;c<64;c++){
		for (let p=0;p<plateau.cases[c].P.length;p++){
			if (plateau.cases[c].P[p][1].type==NP.type && plateau.cases[c].P[p][1].couleur==NP.couleur){
				g.push(plateau.cases[c].P[p][1].id);
			}
		}
	}
	
	g.push(-1)
	let m=Math.max(g);
	if (m<0){
		return NP;
	} else {
		return new Piece(NP.type,NP.couleur,m+1)
	}
	
	
}



function Matt(){
	return;
}

function Patt(){
	return;
}


function contient(Case,type,couleur,id){
	//alert(Case);
	if (type==null && couleur==null && id==null){
		return false;
	} else if (type!=null && couleur==null && id==null){
		for (let i=0;i<plateau.cases[Case].P.length;i++){
			if (plateau.cases[Case].P[i][1].type==type){
				return [true,i];
			}
		}
	} else if (type==null && couleur!=null && id==null){
		for (let i=0;i<plateau.cases[Case].P.length;i++){
			if (plateau.cases[Case].P[i][1].couleur==couleur){
				return [true,i];
			}
		}
	} else if(type==null && couleur==null && id!=null){
		for (let i=0;i<plateau.cases[Case].P.length;i++){
			if (plateau.cases[Case].P[i][1].id==id){
				return [true,i];
			}
		}
	} else if(type!=null && couleur==null && id!=null){
		for (let i=0;i<plateau.cases[Case].P.length;i++){
			if (plateau.cases[Case].P[i][1].id==id && plateau.cases[Case].P[i][1].type==type){
				return [true,i];
			}
		}
	} else if(type==null && couleur!=null && id!=null){
		for (let i=0;i<plateau.cases[Case].P.length;i++){
			if (plateau.cases[Case].P[i][1].id==id && plateau.cases[Case].P[i][1].couleur==couleur){
				return [true,i];
			}
		}
	} else if(type!=null && couleur!=null && id==null){
		for (let i=0;i<plateau.cases[Case].P.length;i++){
			if (plateau.cases[Case].P[i][1].couleur==couleur && plateau.cases[Case].P[i][1].type==type){
				return [true,i];
			}
		}
	} else {
		for (let i=0;i<plateau.cases[Case].P.length;i++){
			if (plateau.cases[Case].P[i][1].couleur==couleur && plateau.cases[Case].P[i][1].type==type && plateau.cases[Case].P[i][1].id==id){
				return [true,i];
			}
		}
	}
	return [false,null];
	
}





//---------------------------------------chronos--------------------------------------------------



//Blanc
var sec_B = 0;
var min_B = 2;
var hrs_B = 0;
var t_B;
let temps_def = [hrs_B,min_B,sec_B] ;
var running_B = false;

var h1_B = document.getElementById('TimerB');
var start_B = document.getElementById('strtB');
var stop_B = document.getElementById('stpB');

//Noir
var sec_N = 0;
var min_N = 2;
var hrs_N = 0;
var t_N;
var running_N = false;

var h1_N = document.getElementById('TimerN');
var start_N = document.getElementById('strtN');
var stop_N = document.getElementById('stpN');




function set_Chr() {
	sec_B = prompt('secondes :'); min_B = prompt('minutes :'); hrs_B = prompt('heures :');
	sec_N = sec_B ; min_N = min_B ; hrs_N = hrs_B ;
	temps_def = [hrs_B,min_B,sec_B] ;
	console.log(temps_def);
    h1_B.textContent = (hrs_B > 9 ? hrs_B : "0" + hrs_B) + ":" + (min_B > 9 ? min_B : "0" + min_B) + ":" + (sec_B > 9 ? sec_B : "0" + sec_B);
	h1_N.textContent = (hrs_N > 9 ? hrs_N : "0" + hrs_N) + ":" + (min_N > 9 ? min_N : "0" + min_N) + ":" + (sec_N > 9 ? sec_N : "0" + sec_N);
}


function timer_B() {
    t_B = setTimeout(add_B, 1000);
}

function tick_B(){
	if (hrs_B+min_B+sec_B == 0) {
		Stop_B();
	
	} else {
		sec_B--;
		if (sec_B == -1) {
			sec_B = 59;
			min_B--;
			if (min_B == -1) {
				min_B = 59;
				hrs_B--;
			}
        }
    }
}




function add_B() {
    tick_B();
    h1_B.textContent = (hrs_B > 9 ? hrs_B : "0" + hrs_B) 
        	 + ":" + (min_B > 9 ? min_B : "0" + min_B)
       		 + ":" + (sec_B > 9 ? sec_B : "0" + sec_B);
    timer_B();
}

function Start_B() {
	if (running_B == false) {timer_B();}
	running_B=true;
	start_B.style.backgroundColor = 'orange';
	stop_B.style.backgroundColor = '#ff009d';
}

function Stop_B() {
	running_B = false ;
    clearTimeout(t_B);
	start_B.style.backgroundColor = '#ff009d';
	stop_B.style.backgroundColor = 'orange';
}

start_B.onclick = function () {Start_B();};

stop_B.onclick =  function () {Stop_B();};




function timer_N() {
    t_N = setTimeout(add_N, 1000);
}

function tick_N(){
	if (hrs_N+min_N+sec_N == 0) {
		Stop_N();
	
	} else {
		sec_N--;
		if (sec_N == -1) {
			sec_N = 59;
			min_N--;
			if (min_N == -1) {
				min_N = 59;
				hrs_N--;
			}
        }
    }
}



function add_N() {
    tick_N();
    h1_N.textContent = (hrs_N > 9 ? hrs_N : "0" + hrs_N) 
        	 + ":" + (min_N > 9 ? min_N : "0" + min_N)
       		 + ":" + (sec_N > 9 ? sec_N : "0" + sec_N);
    timer_N();
}

function Start_N() {
	if (running_N == false) {timer_N();}
	running_N= true;
	start_N.style.backgroundColor = 'orange';
	stop_N.style.backgroundColor = '#ff009d';
}

function Stop_N() {
	running_N = false;
    clearTimeout(t_N);
	start_N.style.backgroundColor = '#ff009d';
	stop_N.style.backgroundColor = 'orange';
}


start_N.onclick = function () {Start_N();};

stop_N.onclick =  function () {Stop_N();};


function recharge_Chronos() {
	sec_B = temps_def[2];
	min_B = temps_def[1];
	hrs_B = temps_def[0];
	sec_N = sec_B ; min_N = min_B ; hrs_N = hrs_B ;
	h1_B.textContent = (hrs_B > 9 ? hrs_B : "0" + hrs_B) + ":" + (min_B > 9 ? min_B : "0" + min_B) + ":" + (sec_B > 9 ? sec_B : "0" + sec_B);
	h1_N.textContent = (hrs_N > 9 ? hrs_N : "0" + hrs_N) + ":" + (min_N > 9 ? min_N : "0" + min_N) + ":" + (sec_N > 9 ? sec_N : "0" + sec_N);
	
}




// -------------------------------------Autres fonctions-------------------------------------

function recherche_p(c,Type,Couleur){
	let Parr=plateau.cases[c].P;
	for (let i=0;i<Parr.length;i++){
		if (Parr[i][1].type==Type && Parr[i][1].couleur==Couleur){
			return Parr[i];
		}
	}
    return false;
}

function selectionne(C) {
	// prend en parametre une case , affiche les cases séléctionnées et déséléctionnées 
	
	
	
	// Partie affichage 
	
	if ((selections.length)<2) { 
		changement_selection(C);
	} else if (selections.length==2 && nb_intrications<nb_max_intrication) {
		changement_selection(C);
	} else if (C.style.backgroundColor == 'red') {
		changement_selection(C);
	}
	
	
	console.log(selections); // selections[0] = depart  selections[1] = arrivee
}

function changement_selection(C) {
	
	
	
	if (C.style.backgroundColor == 'orange' || C.style.backgroundColor == 'red') {
		
		var CN = document.getElementsByClassName("case_N");
		
		for (var i = 0; i < CN.length; i++) {
			
			if (CN[i] == C) {
				
				C.style.backgroundColor = '#757575';
				selections.pop(C);
						
			}
		}
		
		var CB = document.getElementsByClassName("case_B");
		
		for (var i = 0; i < CB.length; i++) {
			
			if (CB[i] == C) {
				C.style.backgroundColor = 'white';
				
				selections.pop(C);
				
			}
		}	
	
	} else if (selections.length == 0){
		if (informations_case(C).length==0) {
			return ;
		}
		C.style.backgroundColor = 'orange';
		
		if (informations_case(C).length <2) {
			selections.push([informations_case(C)[0],C]);
		} else {
			// demander à l'utilisateur quelle pièce selectionner
			let pc=prompt("donner le numéro de la piece dans l'ordre dans laquelle elle apparait");
			selections.push([informations_case(C)[pc-1],C]);
		}
		
	} else {
		C.style.backgroundColor = 'red';
		selections.push([informations_case(C),C]);
	}
	
}

function informations_case(C) {
	const info=[];
	c = Number(C.id);
	
	if (C.innerHTML!='') {
		nodelist=C.querySelectorAll('img');
		for (let i = 0; i < nodelist.length; i++) {
			
			info.push(nodelist[i]);
		}
		
		if ((info.length)==1) {
			
			image_contenu_case.src = info[0].src;
			image_contenu_case.alt = info[0].id ;
			image_contenu_case.style.height = 55*plateau.cases[c].P[0][0]+'px';
			image_contenu_case.style.width = 55*plateau.cases[c].P[0][0]+'px';
			case_annexe.appendChild(document.createTextNode(C.id+' :'));
			case_annexe.appendChild(image_contenu_case);
			
			
			case_annexe.appendChild(document.createTextNode(plateau.cases[c].P[0][0]));
				
			
			
		} else if ((info.length)==2) {
			case_annexe.appendChild(document.createTextNode(C.id+' :'));
			
			
			let image_contenu_case_2 = document.createElement('img');
			
			image_contenu_case.src = info[0].src;
			image_contenu_case.alt = info[0].id ;
			image_contenu_case.style.height = 55*plateau.cases[c].P[0][0]+'px';
			image_contenu_case.style.width = 55*plateau.cases[c].P[0][0]+'px';
			info[0].style.height = '27.5px';
			info[0].style.width = '27.5px';
			proba=plateau.cases[c].P[0][0];
			case_annexe.appendChild(image_contenu_case);
			case_annexe.appendChild(document.createTextNode(' : '+proba+' ;'));
			
			image_contenu_case_2.src = info[1].src;
			image_contenu_case_2.alt = info[1].id ;
			image_contenu_case_2.style.height = 55*plateau.cases[c].P[1][0]+'px';
			image_contenu_case_2.style.width = 55*plateau.cases[c].P[1][0]+'px';
			info[1].style.height = '27.5px';
			info[1].style.width = '27.5px';
			proba=plateau.cases[c].P[1][0];
			case_annexe.appendChild(image_contenu_case_2);
			case_annexe.appendChild(document.createTextNode(' : '+proba+' ;'));
			
			/* case_annexe.appendChild(document.createTextNode(proba+' '));
			case_annexe.appendChild(document.createTextNode(';')); */
		}
		
		
			
			
			
		
	}	
	C.onmouseout = function() {case_annexe.innerHTML = ''};
	return info;
	
}



function recharge() {
	choix=prompt('Vous êtes sûrs de vouloir recommencer ? (Entrez quelquechose pour annuler)');
	if (choix=='') {
		window.location.reload();
	} else {
		return 'annulé';
	}
}
	
	
function img_Piece(I) {
	let image_piece = I.id;
	// parametre : une piece (<img>) HTML   Retourne une Piece (Class) 
	let id_piece = image_piece[2];
	if (image_piece[1]=='N') {	
		couleur_piece = 'noir';	
	} else {
		couleur_piece = 'blanc';
	}
		
	if ((image_piece[0])=='P') {
		type_piece ='pion';
	} else if (image_piece[0]=='T') {
		type_piece ='tour';	
	} else if (image_piece[0]=='C') {
		type_piece ='cavalier';	
	} else if (image_piece[0]=='F') {
		type_piece ='fou';	
	} else if (image_piece[0]=='R') {
		type_piece ='roi';	
	} else {
		type_piece ='dame';	
	}
	
	
	return new Piece(type_piece,couleur_piece,id_piece);
}
	
function jouer(selections) {
	// Vérification des séléctions
	if (selections.length <2) {
		
		return 'pas assez de séléctions';
	}
	
	
	
	// parametres liste des cases selectionnées , execute la fonction déplace
	let dep = selections[0][1].id;
	let arr = selections[1][1].id;
	if (selections.length==3) {
		arr2 = selections[2][1].id;
	}
	
	let pc_class = img_Piece(selections[0][0]);
	let indice=contient(dep,pc_class.type,pc_class.couleur,null)[1];
	let piece= plateau.cases[dep].P[indice][1];
	let Proba= plateau.cases[dep].P[indice][0];
	
	
	if (selections.length==3) {
		// cas intrication
		//choix de la proba
		if (choisir_probabilités) {
			division = 0;
			while (division > 1 || division<=0) {
				division = prompt(" indiquer la proba pour la case "+selections[1][1].id+"    \n      Attention : Doit être < 1");
			}
		}	
		
		
		//déplacement 
		if (deplace(piece,Number(dep),Number(arr),Number(arr2),false,proba=Proba)) {
			recharge_plateau();

			changement_selection(document.getElementById(arr2));
			changement_selection(document.getElementById(arr));
			changement_selection(document.getElementById(dep));
				
			if (chronos_active) {
				if (couleur_piece=='blanc') {
					Start_N();
					Stop_B();
				
				} else {
					Start_B();
					Stop_N();
				}
				
			} else {
				Stop_N();
				Stop_B();
			}
			tour=tour+1;
			
		}
	} else {
		
	
		
		if (deplace(piece,Number(dep),Number(arr),null,false,Proba)) {
			
			recharge_plateau();

			changement_selection(document.getElementById(arr));
			changement_selection(document.getElementById(dep));
			if (chronos_active) {
				if (couleur_piece=='blanc') {
					Start_N();
					Stop_B();
				
				} else {
					Start_B();
					Stop_N();
				}
				
			} else {
				Stop_N();
				Stop_B();
			}
			tour=tour+1;
			
		}
	}
	
	nb_tour.innerHTML = tour ;
	return ;
	
}


function recharge_plateau() {
	let n=0;
	nb_intrications = 0;
	for (let i=0; i<64; i++) {
		/*Parametre : le plateau JS et le plateau HTML, Modifie le plateau HTML en fonction du plateau JS*/
		
		document.getElementById(i).innerHTML = '';
		
		// récuperer les pieces JS de la case i
		
		let P_js = [];
		for (let j=0; j<plateau.cases[i].P.length; j++) {
			P_js.push(plateau.cases[i].P[j]);
			n= n+1;
		}
		
		
		
		
		// Creer les images HTML en fonction des pieces JS
		
		for (let l=0; l< P_js.length; l++) {
			let P_html = document.createElement('img');
			if (P_js[l][1].couleur == 'noir') {
				
				P_html.src = dico_src_N[P_js[l][1].type]    ;
				P_html.id =  dico_id_N[P_js[l][1].type]+P_js[l][1].id  ;
				P_html.style.height = String(55*P_js[l][0])+'px';
				P_html.style.width = String(55*P_js[l][0])+'px';
				
			} else {
				P_html.src = dico_src_B[P_js[l][1].type]    ;
				P_html.id =  dico_id_B[P_js[l][1].type]+P_js[l][1].id   ;
				P_html.style.height = String(55*P_js[l][0])+'px';
				P_html.style.width = String(55*P_js[l][0])+'px';
			}
			
			
				
			document.getElementById(i).appendChild(P_html);
			if (P_js[l][0]<1) {
				nb_intrications = nb_intrications + 1
			}
		}
		
		
		
	}
	nb_piece.innerHTML = n;
	nb_intric.innerHTML = nb_intrications;
	
	recharge_Chronos();
	
}


function repas_2(Piece,Case){
	for (let c=0; c<plateau.cases[Case].P.length; c++){
		if (plateau.cases[Case].P[c][1].couleur==Piece.couleur){return;}
		
		
		if (Piece.couleur=='blanc'){
			return;
		
		}
	}
}




function repas(P,C) {
	C = document.getElementById(C);
	
	nodelist=C.querySelectorAll('img');
	for (let i = 0; i < nodelist.length; i++) {
		if (P.couleur=='noir') {
			zone_morts_N.appendChild(nodelist[i]);
		} else {
			zone_morts_B.appendChild(nodelist[i]);
		}
	}
}


// ---

function cache_cache(M) {
	if (M.hidden == true) {
		M.hidden = false;
	} else {
		M.hidden = true;
	}
}


// Evenements ----------------------------------------------
recharge_plateau();

c0.onclick  = function() {selectionne(c0)} ;c1.onclick  = function() {selectionne(c1)} ;c2.onclick  = function() {selectionne(c2)} ;c3.onclick  = function() {selectionne(c3)} ;c4.onclick  = function() {selectionne(c4)} ;c5.onclick  = function() {selectionne(c5)} ;c6.onclick  = function() {selectionne(c6)} ;c7.onclick  = function() {selectionne(c7)} ;
c8.onclick  = function() {selectionne(c8)} ;c9.onclick  = function() {selectionne(c9)} ;c10.onclick = function() {selectionne(c10)};c11.onclick = function() {selectionne(c11)};c12.onclick = function() {selectionne(c12)};c13.onclick = function() {selectionne(c13)};c14.onclick = function() {selectionne(c14)};c15.onclick = function() {selectionne(c15)};
c16.onclick = function() {selectionne(c16)};c17.onclick = function() {selectionne(c17)};c18.onclick = function() {selectionne(c18)};c19.onclick = function() {selectionne(c19)};c20.onclick = function() {selectionne(c20)};c21.onclick = function() {selectionne(c21)};c22.onclick = function() {selectionne(c22)};c23.onclick = function() {selectionne(c23)};
c24.onclick = function() {selectionne(c24)};c25.onclick = function() {selectionne(c25)};c26.onclick = function() {selectionne(c26)};c27.onclick = function() {selectionne(c27)};c28.onclick = function() {selectionne(c28)};c29.onclick = function() {selectionne(c29)};c30.onclick = function() {selectionne(c30)};c31.onclick = function() {selectionne(c31)};
c32.onclick = function() {selectionne(c32)};c33.onclick = function() {selectionne(c33)};c34.onclick = function() {selectionne(c34)};c35.onclick = function() {selectionne(c35)};c36.onclick = function() {selectionne(c36)};c37.onclick = function() {selectionne(c37)};c38.onclick = function() {selectionne(c38)};c39.onclick = function() {selectionne(c39)};
c40.onclick = function() {selectionne(c40)};c41.onclick = function() {selectionne(c41)};c42.onclick = function() {selectionne(c42)};c43.onclick = function() {selectionne(c43)};c44.onclick = function() {selectionne(c44)};c45.onclick = function() {selectionne(c45)};c46.onclick = function() {selectionne(c46)};c47.onclick = function() {selectionne(c47)};
c48.onclick = function() {selectionne(c48)};c49.onclick = function() {selectionne(c49)};c50.onclick = function() {selectionne(c50)};c51.onclick = function() {selectionne(c51)};c52.onclick = function() {selectionne(c52)};c53.onclick = function() {selectionne(c53)};c54.onclick = function() {selectionne(c54)};c55.onclick = function() {selectionne(c55)};
c56.onclick = function() {selectionne(c56)};c57.onclick = function() {selectionne(c57)};c58.onclick = function() {selectionne(c58)};c59.onclick = function() {selectionne(c59)};c60.onclick = function() {selectionne(c60)};c61.onclick = function() {selectionne(c61)};c62.onclick = function() {selectionne(c62)};c63.onclick = function() {selectionne(c63)};

c0.onmouseover  = function() {informations_case(c0)} ;c1.onmouseover  = function() {informations_case(c1)} ;c2.onmouseover  = function() {informations_case(c2)} ;c3.onmouseover  = function() {informations_case(c3)} ;c4.onmouseover  = function() {informations_case(c4)} ;c5.onmouseover  = function() {informations_case(c5)} ;c6.onmouseover  = function() {informations_case(c6)} ;c7.onmouseover  = function() {informations_case(c7)} ;
c8.onmouseover  = function() {informations_case(c8)} ;c9.onmouseover  = function() {informations_case(c9)} ;c10.onmouseover = function() {informations_case(c10)};c11.onmouseover = function() {informations_case(c11)};c12.onmouseover = function() {informations_case(c12)};c13.onmouseover = function() {informations_case(c13)};c14.onmouseover = function() {informations_case(c14)};c15.onmouseover = function() {informations_case(c15)};
c16.onmouseover = function() {informations_case(c16)};c17.onmouseover = function() {informations_case(c17)};c18.onmouseover = function() {informations_case(c18)};c19.onmouseover = function() {informations_case(c19)};c20.onmouseover = function() {informations_case(c20)};c21.onmouseover = function() {informations_case(c21)};c22.onmouseover = function() {informations_case(c22)};c23.onmouseover = function() {informations_case(c23)};
c24.onmouseover = function() {informations_case(c24)};c25.onmouseover = function() {informations_case(c25)};c26.onmouseover = function() {informations_case(c26)};c27.onmouseover = function() {informations_case(c27)};c28.onmouseover = function() {informations_case(c28)};c29.onmouseover = function() {informations_case(c29)};c30.onmouseover = function() {informations_case(c30)};c31.onmouseover = function() {informations_case(c31)};
c32.onmouseover = function() {informations_case(c32)};c33.onmouseover = function() {informations_case(c33)};c34.onmouseover = function() {informations_case(c34)};c35.onmouseover = function() {informations_case(c35)};c36.onmouseover = function() {informations_case(c36)};c37.onmouseover = function() {informations_case(c37)};c38.onmouseover = function() {informations_case(c38)};c39.onmouseover = function() {informations_case(c39)};
c40.onmouseover = function() {informations_case(c40)};c41.onmouseover = function() {informations_case(c41)};c42.onmouseover = function() {informations_case(c42)};c43.onmouseover = function() {informations_case(c43)};c44.onmouseover = function() {informations_case(c44)};c45.onmouseover = function() {informations_case(c45)};c46.onmouseover = function() {informations_case(c46)};c47.onmouseover = function() {informations_case(c47)};
c48.onmouseover = function() {informations_case(c48)};c49.onmouseover = function() {informations_case(c49)};c50.onmouseover = function() {informations_case(c50)};c51.onmouseover = function() {informations_case(c51)};c52.onmouseover = function() {informations_case(c52)};c53.onmouseover = function() {informations_case(c53)};c54.onmouseover = function() {informations_case(c54)};c55.onmouseover = function() {informations_case(c55)};
c56.onmouseover = function() {informations_case(c56)};c57.onmouseover = function() {informations_case(c57)};c58.onmouseover = function() {informations_case(c58)};c59.onmouseover = function() {informations_case(c59)};c60.onmouseover = function() {informations_case(c60)};c61.onmouseover = function() {informations_case(c61)};c62.onmouseover = function() {informations_case(c62)};c63.onmouseover = function() {informations_case(c63)};

boutton_jouer.onclick = function() {jouer(selections)};
recommencer.onclick = function() {recharge()};

boutton_menu.onclick = function() {cache_cache(menu_opts)};
bouton_opts_avance.onclick = function() {cache_cache(menu_opts_avance)};
max_intric.onclick = function() {let nouveau_max_intric = prompt("entrez le nombre maximal d'intrication désiré");max_intric.innerHTML = nouveau_max_intric; nb_max_intrication = Number(nouveau_max_intric);};
choix_proba.onclick = function() {if (choisir_probabilités) {choisir_probabilités = false;choix_proba.innerHTML = 'désactivé';} else { choisir_probabilités = true;choix_proba.innerHTML = 'activé';}};
activ_chrono.onclick = function() {if (chronos_active) {chronos_active=false;activ_chrono.innerHTML = 'désactivés';} else {chronos_active=true;activ_chrono.innerHTML = 'activés';}};
setChr.onclick = function() {set_Chr()};