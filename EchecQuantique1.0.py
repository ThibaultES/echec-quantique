
# %%

import random

# %%


class Piece:

    def __init__(self,nature,couleur,num):
        self.type=nature
        self.couleur=couleur
        self.id=num

    def __repr__(self):
        return f"{self.type};{self.couleur};{self.id}"

    def deplacementsPos(self):
        if self.type=='pion' and self.couleur=='blanc':
            return [(0),(7),(1)]
        if self.type=='pion' and self.couleur=='noir':
            return [(4),(3),(5)]
        if self.type=='fou' and self.couleur=='blanc':
            return [(7),(1),(3),(5)]
        if self.type=='fou' and self.couleur=='noir':
            return [(3),(5),(7),(1)]
        if self.type=='cavalier' and self.couleur=='blanc':
            return [(0,7),(0,1),(2,1),(2,3),(4,3),(4,5),(6,5),(6,7)]
        if self.type=='cavalier' and self.couleur=='noir':
            return [(4,3),(4,5),(6,5),(6,7),(0,7),(0,1),(2,1),(2,3)]
        if self.type=='tour' and self.couleur=='blanc':
            return [(0),(2),(4),(6)]
        if self.type=='tour' and self.couleur=='noir':
            return [(4),(6),(0),(2)]
        if self.type=='dame' and self.couleur=='blanc':
            return [(7),(0),(1),(2),(3),(4),(5),(6)]
        if self.type=='dame' and self.couleur=='noir':
            return [(3),(4),(5),(6),(7),(0),(1),(2)]
        if self.type=='roi' and self.couleur=='blanc':
            return [(7),(0),(1),(2),(3),(4),(5),(6)]
        if self.type=='roi' and self.couleur=='noir':
            return [(3),(4),(5),(6),(7),(0),(1),(2)]

    def egal(self,other):
        if self.type==other.type and self.couleur==other.couleur and self.id==other.id:
            return True
        return False
# %%

class Case:

    def __init__(self,pieces,num):
        self.P=pieces  #[(proba1(max),piece1),(proba2,piece2)...]
        self.num=num


    def __repr__(self):
        return f'{self.num} : {self.P}'

        

    def ajoutePiece(self,piece,proba):
        self.P.append((proba,piece))
        DepP[piece]=True
            

    def enlevePiece(self,p):
        for i in range(len(self.P)):
            if self.P[i][1].egal(p):
                self.P.pop(i)
                return

# %%


class Plateau:

    def __init__(self,cases):
        self.cases=cases


    def __repr__(self):
        s=''
        for i in range(0,64,8):
            s+=f'{self.cases[i:i+8]}\n'
        return s   

    def voisines(self,c):
        if c==0:
            return [None,None,1,9,8,None,None,None]
        if c==7:
            return [None,None,None,None,15,14,6,None]
        if c==56:
            return [48,49,57,None,None,None,None,None]
        if c==63:
            return [55,None,None,None,None,None,62,54]
        if c in range(1,8):
            return [None,None,c+1,c+9,c+8,c+7,c-1,None]
        if c in range(57,63):
            return [c-8,c-7,c+1,None,None,None,c-1,c-9]
        if c in range(8,56,8):
            return [c-8,c-7,c+1,c+9,c+8,None,None,None]
        if c in range(15,63,8):
            return [c-8,None,None,None,c+8,c+7,c-1,c-9]
        else:
            return [c-8,c-7,c+1,c+9,c+8,c+7,c-1,c-9]

    
    def voisineD(self,c,d):  #0:N 1:NE 2:E 3:SE 4:S 5:SO 6:O 7:NO
        return self.voisines(c)[d]



    def desintrique(self,piece):
        n=[]
        for i in range(64):
            for j in range(len(self.cases[i].P)):
                if self.cases[i].P[j][1]==piece:
                    n.append((i,self.cases[i].P[j][0]))
        s=0
        for k in n: s+=k[1]     
        assert(s<=1)
            
        r=random.random()
        
        if len(n)==0:
            return
    
        elif len(n)>0:

            g=False
            h,a,b=0,0,0
            while not g and h<len(n):
                a=b
                b+=n[h][1]
                if r>=a and r<b: g=True
                h+=1
            h-=1

            if g:
                self.cases[n[h][0]].P=[(1,piece)]

                for t in range(len(n)):
                    if t != h:
                        self.cases[n[t][0]].enlevePiece(piece)
            else:
                for t in range(len(n)):
                    self.cases[n[t][0]].enlevePiece(piece)



    def ajouterPiece(self,c,piece,proba):
        
        if len(self.cases[c].P)==0:
            self.cases[c].ajoutePiece(piece,proba)
            return


        for i in range(len(self.cases[c].P)):
            if self.cases[c].P[i][1]==piece:
                self.cases[c].P[i]=(self.cases[c].P[i][0]+proba,self.cases[c].P[i][1])
                if len(self.cases[c].P)==2:
                    j=self.cases[c].P[i][0]+self.cases[c].P[1-i][0]
                    if j>1:
                        self.cases[c].P[1-i]=(self.cases[c].P[1-i][0]-(j-1),self.cases[c].P[1-i][1])
                    if self.cases[c].P[1-i][0]<=0:
                        self.cases[c].enlevePiece(self.cases[c].P[1-i][1])
                return
            
        if len(self.cases[c].P)==1 and self.cases[c].P[0][0]==1:
            Piece2=self.cases[c].P[0][1]
            if Piece2.couleur==piece.couleur:
                return "Can't eat"
            self.cases[c].P=[]
            self.cases[c].ajoutePiece(piece,proba)
            if proba<1:
                self.cases[c].ajoutePiece(Piece2,1-proba)
            return
            
        else:
            self.desintrique(self.cases[c].P[0][1])
            self.ajouterPiece(c,piece,proba)
            return

        
    def deplacerPiece(self,piece,proba,depart,arrive):
        self.cases[depart].enlevePiece(piece)
        a=self.ajouterPiece(arrive,piece,proba)
        if a=="Can't eat":
            self.ajouterPiece(depart,piece,proba)
        return a


def CreePiece(case,piece,proba):
    plateau.ajouterPiece(case,piece,proba)
    DepP[piece]=False
    if piece.type=='roi':
        if piece.couleur=='blanc':
            Echec[roi_B]=False
        else :
            Echec[roi_N]=False
        


def plateauVide():
    C=[]
    for i in range(64):
        C.append(Case([],i))
    return Plateau(C)
        

def plateauDepart():
    for i in range(0,8): CreePiece(i+8,Piece('pion','noir',i),1)
    for i in range(2): CreePiece([0,7][i],Piece('tour','noir',i),1)
    for i in range(2): CreePiece([1,6][i],Piece('cavalier','noir',i),1)
    for i in range(2): CreePiece([2,5][i],Piece('fou','noir',i),1)
    CreePiece(3,Piece('dame','noir',0),1)
    CreePiece(4,roi_N,1)
    
    for i in range(0,8): CreePiece(i+48,Piece('pion','blanc',i),1)
    for i in range(2): CreePiece([56,63][i],Piece('tour','blanc',i),1)
    for i in range(2): CreePiece([57,62][i],Piece('cavalier','blanc',i),1)
    for i in range(2): CreePiece([58,61][i],Piece('fou','blanc',i),1)
    CreePiece(59,Piece('dame','blanc',0),1)
    CreePiece(60,roi_B,1)
    
    return plateau

# %%

# -------------------------------------------------------------

def intrication(piece,proba,depart,arrivee1,arrivee2):
    if piece.type=="roi": return
    a=plateau.deplacerPiece(piece,proba/2,depart,arrivee1)
    if a=="can't eat": return a
    return plateau.deplacerPiece(piece,proba/2,depart,arrivee2)  


def deplace(piece,depart,arrivee,arrivee2=None,essai=False):

    if depart==arrivee or depart==arrivee2: return False
    
    a=''  
    Possible=[]

    proba=0
    for i in range(len(plateau.cases[depart].P)):
        if plateau.cases[depart].P[i][1].egal(piece):
            proba=plateau.cases[depart].P[i][0]

    if piece.type=='pion': #PION

        for i in piece.deplacementsPos():
            Possible.append(plateau.voisineD(depart,i))
            
        if piece.couleur=='blanc':
            Possible.append(plateau.voisineD(plateau.voisineD(depart,0),0))
        else:
            Possible.append(plateau.voisineD(plateau.voisineD(depart,4),4))

        
        if not fonctionPionPeut(piece,Possible,depart,arrivee)[0]: return False
        if fonctionPionPeut(piece,Possible,depart,arrivee)[1]=='double':
            if not essai: DernierCoup['pionDouble']=True
            
        if fonctionPionPeut(piece,Possible,depart,arrivee)[1]=='PEP':
            if piece.couleur=='blanc':
                if not essai: plateau.cases[plateau.voisineD(arrivee,4)].enlevePiece(DernierCoup['piece'])
            elif piece.couleur=='noir':
                if not essai: plateau.cases[plateau.voisineD(arrivee,0)].enlevePiece(DernierCoup['piece'])
                

        if arrivee2 != None:
            if not fonctionPionPeut(piece,Possible,depart,arrivee2)[0]:
                return False
            else:
                if fonctionPionPeut(piece,Possible,depart,arrivee2)[1]=='double':
                    if not essai: DernierCoup['pionDouble2']=True
                    
                if fonctionPionPeut(piece,Possible,depart,arrivee2)[1]=='PEP':
                    if piece.couleur=='blanc':
                        if not essai: plateau.cases[plateau.voisineD(arrivee,4)].enlevePiece(DernierCoup['piece'])
                    elif piece.couleur=='noir':
                        if not essi: plateau.cases[plateau.voisineD(arrivee,0)].enlevePiece(DernierCoup['piece'])

                    
                if not essai: a=intrication(piece,proba,depart,arrivee,arrivee2)
           
        else :
            if not essai: a=plateau.deplacerPiece(piece,proba,depart,arrivee)

        if a=="Can't eat":
            return False
    
        else:
            DernierCoup['piece']=piece
            DernierCoup['proba']=proba
            DernierCoup['depart']=depart
            DernierCoup['arrivee']=arrivee
            if arrivee2!=None:
                DernierCoup['arrivee2']=arrivee2
            return True

    elif piece.type=='roi': # ROI
        #Déplacement simple#
        for i in piece.deplacementsPos(): #i prend la valeur de chaque direction des voisines de depart

        
            case=plateau.voisineD(depart,i)
            Possible.append(case)

        
        if arrivee in Possible : #vérifie si la case d'arrivée est une voisine de la case de départ dans les directions
                                                                        #possibles par un roi                                            
            if not essai : a=plateau.deplacerPiece(piece,proba,depart,arrivee)
            if a=="Can't eat":
                return False
            else:
                DernierCoup['piece']=piece
                DernierCoup['proba']=proba
                DernierCoup['depart']=depart
                DernierCoup['arrivee']=arrivee
                if arrivee2!=None:
                    DernierCoup['arrivee2']=arrivee2
                return True

        else :
            
            #Rock#
            # :  tour sur l'arrivée, pas en échec ni les cases ou ils passe, ne doit pas avoir bougé (tour et roi), pas de pieces entre,
            if rock(piece,depart,arrivee):
                DernierCoup['piece']=piece
                DernierCoup['proba']=proba
                DernierCoup['depart']=depart
                DernierCoup['arrivee']=arrivee
                if arrivee2!=None:
                    DernierCoup['arrivee2']=arrivee2
                return True
            else: return False
                    
            

    elif piece.type=='cavalier': # CAVALIER

        for i in piece.deplacementsPos():
            if plateau.voisineD(depart,i[0])!=None :
                Possible.append(plateau.voisineD(plateau.voisineD(depart,i[0]),i[1]))
            
            
        if arrivee in Possible:

            if arrivee2 ==None:
                if not essai :a=plateau.deplacerPiece(piece,proba,depart,arrivee)
            elif arrivee2 in Possible:
                if not essai: a=intrication(piece,proba,depart,arrivee,arrivee2)
            else:
                return False
            if a=="Can't eat":
                return False
    
            else:
                DernierCoup['piece']=piece
                DernierCoup['proba']=proba
                DernierCoup['depart']=depart
                DernierCoup['arrivee']=arrivee
                if arrivee2!=None:
                    DernierCoup['arrivee2']=arrivee2
                return True
            
        else: return False
        
    else: # FOU TOUR DAME
        
        for i in piece.deplacementsPos(): # pour chaque direction :
            case=depart
            while case != None : #tant que case a une voisine :
                Possible.append(case)
                case=plateau.voisineD(case,i)
                
        if arrivee in Possible:

            if arrivee2==None:
                if not essai : a=plateau.deplacerPiece(piece,proba,depart,arrivee)
            elif arrivee2 in Possible:
                if not essai : a=intrication(piece,proba,depart,arrivee,arrivee2)
            else:
                return False
            
            if a=="Can't eat":
                return False
    
            else:
                DernierCoup['piece']=piece
                DernierCoup['proba']=proba
                DernierCoup['depart']=depart
                DernierCoup['arrivee']=arrivee
                if arrivee2!=None:
                    DernierCoup['arrivee2']=arrivee2  
                return True

        else: return False


def rock(piece,depart,arrivee):
    if not DepP[piece]:
    
        #verif arrivee bonne case#
        if not arrivee in [depart-4,depart+3]:
            return False

        #vérif tour sur arrivee#
        T=recherche_p(arrivee,'tour',piece.couleur)
        
        if T==False:
            return False
        if T[0]!=1:return False
        

        else :
            if len(plateau.cases[arrivee].P)>1 :
                return False

        
        #vérif echec#
        if echec(depart,piece.couleur) :
            
            return False
                    
        if arrivee==depart-4:
            if echec(depart-1,piece.couleur) or echec(depart-2,piece.couleur) :
                return False
        elif arrivee==depart+3:
            if echec(depart+1,piece.couleur) or echec(depart+2,piece.couleur) :
            
                return False
        
        #vérif piece entre#
        if arrivee==depart-4 :
            for i in [depart-3,depart-2,depart-1]:
                if plateau.cases[i].P != [] :
                    return False
        elif arrivee==depart+3 :
            for i in [depart+1,depart+2]:
                if plateau.cases[i].P != [] :
                    return False
        
        
        if arrivee==depart+3:
            plateau.deplacerPiece(piece,1,depart,arrivee-1)
            deplace(T,arrivee,depart+1)
            return True
    
        if arrivee==depart-4:
            plateau.deplacerPiece(piece,1,depart,arrivee+2)
            deplace(T,arrivee,depart-1)
            return True
        
    else : return False


def echec(case,couleur):
    for c in range(len(plateau.cases)):
        for p in plateau.cases[c].P:
            if p[1].couleur!=couleur:
                if deplace(p[1],c,case,essai=True):
                    return True
    return False
    

def fonctionPionPeut(piece,possible,d,a):
    g=False
    A=0

    if a==possible[0]:
        return (True,None)
        
    if a==possible[3] and ((piece.couleur=='blanc' and d in range(48,56) ) or (piece.couleur=='noir' and d in range(8,16) )):
        return (True,'double')
    
    if DernierCoup['pionDouble']:
        A=DernierCoup['arrivee']
        g=True
    elif DernierCoup['pionDouble2']:
        A=DernierCoup['arrivee2']
        g=True
    else: return (False,None)

    if g:
        if (d==plateau.voisineD(A,2) or d==plateau.voisineD(A,6) ) and piece.couleur!=DernierCoup['piece'].couleur:
            if (piece.couleur=='blanc' and a==plateau.voisineD(A,0)) or (piece.couleur=='noir' and a==plateau.voisineD(A,4)):
                return (True,'PEP')

    if a==possible[1]:
        for i in range(len(plateau.cases[possible[1]].P)):
            if plateau.cases[possible[1]].P[i][1].couleur != piece.couleur:
                return (True,None)
        
    if a==possible[2]:
        for i in range(len(plateau.cases[possible[2]].P)):
            if plateau.cases[possible[2]].P[i][1].couleur != piece.couleur:
                return (True,None)
            
    return (False,None)


def recherche_p(c,Type,Couleur) :
    '''recherche une piece sur une case c, return True ou False'''
    
    Parr=plateau.cases[c].P
                
    for i in range(len(Parr)):
        if Parr[i][1].type==Type and Parr[i][1].couleur==Couleur:
            return Parr[i]
    return False


global DepP 
DepP={} #La piece a déjà bougée ?

global roi_N
roi_N=Piece('roi','noir',0)

global roi_B
roi_B=Piece('roi','blanc',0)

global Echec 
Echec={} #Le roi est en échec : Echec[('roi','<couleur>')]=True

global DernierCoup
DernierCoup={'piece':"",'proba':"",'depart':"",'arrivee':"",'arrivee2':None,'pionDouble':False,'pionDouble2':False}

global plateau
plateau=plateauVide()

#---------------------------------------------------------------------------
#-------------------------tests---------------------------------------------
#---------------------------------------------------------------------------

if __name__=='__main__':


    jeu=eval(input('plateau : (vide[0] ou depart[1] ou testTibo[2] ou testThomas[3]) : '))

        
    if jeu=='testThomas' or jeu==3:
        
        #tour1=Piece('tour','blanc',34) ; depart_tour1=2 ; arr_tour1=0 ; CreePiece(2,tour1,1)
        
        tour2=Piece('tour','blanc',21) ; depart_tour2=57;arr_tour2=56; CreePiece(depart_tour2,tour2,1)
        #tour3=Piece('tour','noir',4) ; depart_tour3=54; arr_tour3=54 ; CreePiece(depart_tour3,tour3,1)
        
        #fou1=Piece('fou','blanc',29) ; depart_fou1=51 ; arr_fou1=42 ; CreePiece(depart_fou1,fou1,1)
        
        #roi1=Piece('roi','noir',0) ; depart_roi1=38 ; arr_roi1=30 ; CreePiece(depart_roi1,roi1,1)
        
        roi2=Piece('roi','blanc',1) ; depart_roi2=60 ; arr_roi2=56 ; CreePiece(depart_roi2,roi2,1)
        
        #pion1=Piece('pion','blanc',1);depart_pion1=0;arr_pion1=0; CreePiece(depart_pion1,pion1,1)
        #pion2=Piece('pion','blanc',5);depart_pion2=54;arr_pion2=47; CreePiece(depart_pion2,pion2,1)
        
        cava1=Piece('cavalier','noir',0);depart_cava1=28;arr_cava1=22; CreePiece(depart_cava1,cava1,1)
    
        print("Plateau de Départ :")
        print()
        print(plateau)
        print()

        deplace(tour2,depart_tour2,arr_tour2,49)
        #deplace(tour1,depart_tour1,arr_tour1)
        #deplace(tour3,depart_tour3,arr_tour3)

        #deplace(fou1,depart_fou1,arr_fou1)

        #deplace(roi1,depart_roi1,arr_roi1)

        '''deplace(pion1,depart_pion1,arr_pion1)
        deplace(pion2,depart_pion2,arr_pion2)

        deplace(cava1,depart_cava1,arr_cava1)
        '''
        deplace(roi2,depart_roi2,arr_roi2) # test rock

        #print(echec(63,tour3))

        print("Plateau d'arrivée :")
        print()
        print(plateau)
        print()

        #print(possible_tour(tour1,depart_tour1,arr_tour1))

    elif jeu=='depart' or jeu==1:
        
        plateau=plateauDepart()
        
        print("Plateau de Départ :")
        print()
        print(plateau)
        print()
        
    elif jeu=='testTibo' or jeu==2:

        CreePiece(11,Piece('pion','noir',1),1)
        CreePiece(26,Piece('pion','blanc',2),1)
        CreePiece(17,Piece('tour','blanc',1),1)
        CreePiece(25,Piece('fou','blanc',1),1)
        
        print()
        print(plateau)
        print()
        
        deplace(Piece('pion','noir',1),11,19,27)
        deplace(Piece('tour','blanc',1),17,25)
        print()
        print(plateau)
        print()
        deplace(Piece('pion','blanc',2),26,19)
        
        print()
        print(plateau)
        print()
